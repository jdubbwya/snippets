#!/usr/bin/env bash

function encode_base64_url {
  local input
  if [[ -p /dev/stdin ]]; then
    input="$(cat -)"
  else
    input="${@}"
  fi

  printf "%s" "${input}" | base64 | sed s/\+/-/g | sed 's/\//_/g' | sed -E s/=+$// | tr -d \\n
}

function main {

 
  local algo="RS512"
  local key=""

  local header=""
  local payload=""

  while [ $# -gt 0 ] ;
  do

      case $1 in
          --algo )
              shift
              algo=$1
          ;;
          --header )
              shift
              header="$(printf "%s" "$1" | tr -d "\n" | encode_base64_url)"
          ;;
          --payload )
              shift
              payload="$(printf "%s" "$1" | tr -d "\n" | encode_base64_url)"
          ;;
          --key )
              shift
              key=$1
      esac

      shift

  done

  if [ -z "$header" ]
  then
    printf "A header must be supplied.\n" 1>&2
    exit 1
  fi

  local openssl_algo=""
  case $algo in
      RS256 )
          shift
          openssl_algo="-sha256"
      ;;
      RS384 )
          shift
          openssl_algo="-sha384"
      ;;
      RS512 )
          shift
          openssl_algo="-sha512"
      ;;
      ES256 )
          shift
          openssl_algo="-sha256"
      ;;
      ES384 )
          shift
          openssl_algo="-sha384"
      ;;
      ES512 )
          shift
          openssl_algo="-sha512"
      ;;
      * )
          printf "%s is not a known algorithm" "$algo" 1>&2
          exit 2
  esac

  local prefix=$(printf '%s.%s' "${header}" "${payload}")

  # Calculate hmac signature -- note option to pass in the key as hex bytes
  signature=$(printf '%s' "${prefix}" | openssl dgst $openssl_algo -binary -sign $key | encode_base64_url)

  # Create the full token
  printf "%s.%s" "${prefix}" "${signature}"

}

main "$@"
