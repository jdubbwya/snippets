#!/usr/bin/env sh
# See https://jamielinux.com/docs/openssl-certificate-authority/index.html for additional references

function gen_authority {

    if [ -d $CA_ROOT_DIR]
    then
        rm -rf $CA_ROOT_DIR
    fi

    mkdir -p $CA_ROOT_DIR
    pushd $CA_ROOT_DIR
    mkdir certs crl newcerts private
    chmod 700 private
    touch index.txt
    echo 1000 > serial
    popd

    CA_KEY_PATH="${CA_ROOT_DIR}/private/ca.key.pem"
    CA_KEY_PASSPHRASE="${CA_ROOT_DIR}/private/ca.key.passphrase"
    CA_CERT_PATH="${CA_ROOT_DIR}/ca.cert.pem"
    CA_CONF="${CA_ROOT_DIR}/openssl.conf"

    echo "$(export LC_ALL=C; cat /dev/urandom | tr -dc '(\&\_a-zA-Z0-9\^\*\@' | fold -w ${1:-32} | head -n 1)" >> $CA_KEY_PASSPHRASE

    cat <<- REQ_CONF_END > $CA_CONF
[ ca ]
# \`man ca\`
default_ca = CA_default

[ CA_default ]
# Directory and file locations.
dir               = ${CA_ROOT_DIR}
certs             = ${CERTS_DIR}
crl_dir           = \$dir/crl
new_certs_dir     = \$dir/newcerts
database          = \$dir/index.txt
serial            = \$dir/serial
RANDFILE          = \$dir/private/.rand

# The root key and root certificate.
private_key       = \$dir/private/ca.key.pem
certificate       = ${CERTS_DIR}/ca.cert.pem

# For certificate revocation lists.
crlnumber         = \$dir/crlnumber
crl               = \$dir/crl/ca.crl.pem
crl_extensions    = crl_ext
default_crl_days  = 30

# SHA-1 is deprecated, so use SHA-2 instead.
default_md        = sha256

name_opt          = ca_default
cert_opt          = ca_default
default_days      = 375
preserve          = no

policy            = policy_loose

[ policy_loose ]
# Allow the intermediate CA to sign a more diverse range of certificates.
# See the POLICY FORMAT section of the `ca` man page.
countryName             = optional
stateOrProvinceName     = optional
organizationName        = optional
organizationalUnitName  = optional
commonName              = supplied
emailAddress            = optional

[ req ]
# Options for the \`req\` tool (\`man req\`).
default_bits        = 2048
distinguished_name  = req_distinguished_name
string_mask         = utf8only

# SHA-1 is deprecated, so use SHA-2 instead.
default_md          = sha256

[ req_distinguished_name ]
# See <https://en.wikipedia.org/wiki/Certificate_signing_request>.
countryName                     = Country Name (2 letter code)
stateOrProvinceName             = State or Province Name
organizationName                = Organization Name
organizationalUnitName          = Organizational Unit Name
commonName                      = Common Name

[ v3_ca ]
# Extensions for a typical intermediate CA (\`man x509v3_config\`).
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid:always,issuer
basicConstraints = critical, CA:true
keyUsage = critical, digitalSignature, cRLSign, keyCertSign

[ usr_cert ]
# Extensions for client certificates (\`man x509v3_config\`).
basicConstraints = CA:FALSE
nsCertType = client, email
nsComment = "OpenSSL Generated Client Certificate"
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid,issuer
keyUsage = critical, nonRepudiation, digitalSignature, keyEncipherment
extendedKeyUsage = clientAuth, emailProtection

[ server_cert ]
# Extensions for server certificates (\`man x509v3_config\`).
basicConstraints = CA:FALSE
nsCertType = server
nsComment = "OpenSSL Generated Server Certificate"
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid,issuer:always
keyUsage = critical, digitalSignature, keyEncipherment
extendedKeyUsage = serverAuth

[ crl_ext ]
# Extension for CRLs (\`man x509v3_config\`).
authorityKeyIdentifier=keyid:always

REQ_CONF_END

    local ca_csr_path="${CA_ROOT_DIR}/ca.csr"

    if ! openssl genrsa -aes256 -out $CA_KEY_PATH -passout "file:$CA_KEY_PASSPHRASE" 4096
    then
        printf "\n[FATAL]\tUnable to generate key\n" 1>&2
        exit 1
    fi

    chmod 400 $CA_KEY_PATH

    if ! openssl req -x509 -new -nodes -sha256 -days 7300 -config $CA_CONF -extensions v3_ca \
        -subj "/C=US/ST=California/O=Anon/OU=Development/CN=authority" \
        -passin "file:$CA_KEY_PASSPHRASE" \
        -key $CA_KEY_PATH \
        -out $CA_CERT_PATH
    then
        printf "\n[FATAL]\tUnable to generate certificate\n" 1>&2
        exit 1
    fi

}

function gen_server_cert {

    local csr_path="${CERTS_DIR}/mysql.csr"
    local key_path="${CERTS_DIR}/mysql.key.pem"
    local cert_path="${CERTS_DIR}/mysql.cert.pem"

    if ! openssl req -newkey "rsa:2048" -nodes -config $CA_CONF \
        -keyout $key_path \
        -out $csr_path \
        -subj "/C=US/ST=California/O=Anon/OU=Development/CN=server"
    then
        printf "\n[FATAL]\tUnable to generate key and certificate request\n" 1>&2
        exit 1
    fi

    if ! openssl ca -config $CA_CONF -days 375 -notext -md sha256  \
        -extensions server_cert -batch \
        -passin "file:$CA_KEY_PASSPHRASE" \
        -in $csr_path \
        -out $cert_path
    then
        printf "\n[FATAL]\tUnable to generate pem certificate\n" 1>&2
        exit 1
    fi
}

function gen_client_cert {

    local csr_path="${CERTS_DIR}/etl.csr"
    local key_path="${CERTS_DIR}/etl.key.pem"
    local cert_path="${CERTS_DIR}/etl.cert.pem"

    if ! openssl req -newkey "rsa:2048" -nodes -config $CA_CONF \
        -keyout $key_path \
        -out $csr_path \
        -subj "/C=US/ST=California/O=Anon/OU=Development/CN=client"
    then
        printf "\n[FATAL]\tUnable to generate key and certificate request\n" 1>&2
        exit 1
    fi

    if ! openssl ca -config $CA_CONF -days 375 -notext -md sha256  \
        -extensions usr_cert -batch \
        -passin "file:$CA_KEY_PASSPHRASE" \
        -in $csr_path \
        -out $cert_path
    then
        printf "\n[FATAL]\tUnable to generate pem certificate\n" 1>&2
        exit 1
    fi
}

function main {

    CA_ROOT_DIR="$(pwd)/ca"
    CERTS_DIR="$(pwd)/certs"

    gen_authority
    gen_server_cert
    gen_client_cert

}

main $@