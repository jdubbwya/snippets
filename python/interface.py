import abc
import logging

logger = logging.getLogger('snippets.python.interface')

class SampleInterface(metaclass=abc.ABCMeta):
    @classmethod
    def __subclasshook__(cls, subclass):
        return (hasattr(subclass, 'method_one') and 
                callable(subclass.load_data_source) and 
                hasattr(subclass, 'method_two') and 
                callable(subclass.extract_text) or 
                NotImplemented)

    @abc.abstractmethod
    def method_one(self, a: str, b: str):
        """First Method"""
        raise NotImplementedError

    @abc.abstractmethod
    def method_two(self, a: str):
        """Second method"""
        raise NotImplementedError


class SampleImplmentation(SampleInterface):
    """ Sample Class Implementing interface """
    def method_one(self, a: str, b: str):
         """First Method"""
        logger.debug("method_one: %s, %s", a, b)

    def method_two(self, a: str):
         """Second method"""
         logger.debug("method_two: %s")