import logging

# Obtain root logger
LOGGER = logging.getLogger('')

# Set logging level to allow all then let each handler set their level independently
LOGGER.setLevel(logging.NOTSET)

## Appending File Handler
fh = logging.FileHandler(filename="/var/logs/python.log", mode='a')

# Format the log messages
fh.setFormatter( logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s') )

# Add to root logger
LOGGER.addHandler(fh)


## Console Handler
ch = logging.StreamHandler()

# Format the log messages
ch.setFormatter( logging.Formatter( '%(levelname)s - %(message)s') )

# Add to root logger
LOGGER.addHandler(ch)

