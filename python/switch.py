import logging
import sys

LOGGER = logging.getLogger('')

def main():
    """ Main function """

    def inner():
        """ Inner namespace to collect functions """

        def case_one():
            LOGGER.info("case one called")
        #end case_one

        def case_two():
            LOGGER.info("case two called")
        #end case_two

        def default():
            """ Using this to illustrate how to fail when case does not match """
            raise Exception()
        #end default

        func = locals().get(sys.argv[1], default)
        func()

    #end inner

    inner()
    exit(0)

#end main

if __name__ == '__main__':
	main()
