import {Socket} from "node:net"

/**
 * 
 * @param {int} port 
 * @param {string} address 
 * @returns {Promise<[int, string]>}
 */
export default function (port, address = "localhost") {
    return new Promise((resolve, reject) => {
        const sock = new Socket()
        sock.connect(port, address)

        sock.on('connect', () => { 
            reject(new Error("Connection was established to existing resource"))
        })
        sock.on('error', (e) => {
            resolve([port, address])
        })

        sock.end();
    })
}