import readline from 'node:readline'

function main() {

    console.log("Starting application use control+d to exit.")
    readline.emitKeypressEvents(process.stdin)
    if (process.stdin.setRawMode != null) {
        process.stdin.setRawMode(true)
        process.stdin.resume()
    }

    process.stdin.on("keypress", function (str, key) {
        if (key && key.ctrl && ["c", "d"].indexOf(key.name) !== -1) {
            console.log("Shuting down")
            process.stdin.pause()
        }
    });
}

main()