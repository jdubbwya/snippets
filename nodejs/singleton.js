const instance = Symbol.for("Singleton")

class Singleton {

  test(){
      return true
  }

}

if ( !globalThis[instance] ) {
  globalThis[instance] = new Singleton()
}


module.exports = exports = globalThis[instance]
