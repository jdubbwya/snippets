
local Parent = {}

function Parent:new()

  local o = {}
  setmetatable(o, self)
  self.__index = self
  self.class_name = "Parent"
  return o

end

function Parent:printClassName()
  print(self.class_name .. ":printClassName")
end

local Child = Parent:new()

function Child:new()
	local o = Parent.new(self)
	self.class_name = "Child"
	return o
end

function Child:printChild()
  print("Child:printChild")
end


local parent = Parent:new()
parent:printClassName()

local child = Child:new()
child:printClassName()
child:printChild()
