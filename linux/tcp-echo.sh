#!/usr/bin/env bash

function main {

    local port=$1

    if [ $# -lt 1 ] || [ -z $port ]
    then
        printf "A port MUST be supplied.\ntcp-echo.sh <port>" 1>&2
        exit 2
    fi

    ncat -l $port --keep-open --exec "/bin/cat"
}

main $@
